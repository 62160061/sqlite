package com.mycompany.sqlite01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ModG
 */
public class CreateTable {
public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        //Connectoin
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            stmt = conn.createStatement();
            String sql = "CREATE TABLE USER" +
                    "(ID INT PRIMARY KEY NOT NULL,"
                    + "FIRSTNAME TEXT NOT NULL, " + 
                    "LASTNAME TEXT NOT NULL,"+ 
                    "AGE INT NOT NULL,"+
                     "BIRTHDAY TEXT NOT NULL";

            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC!!!!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database!!");
            System.exit(0);
        }

    }
}
