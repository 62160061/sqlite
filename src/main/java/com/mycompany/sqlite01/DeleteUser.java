/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sqlite01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ModG
 */
public class DeleteUser {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn=DriverManager.getConnection("jdbc:sqlite:user.db");  
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            stmt.executeUpdate("DELETE from USER where ID =2");
            conn.commit();
            //select
            ResultSet rs =stmt.executeQuery("SELECT * FROM USER");
            while(rs.next()){
             int id = rs.getInt("id");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            int age = rs.getInt("age");
            String birthday = rs.getString("birthday");
            System.out.println("ID = "+id);
            System.out.println("NAME = "+firstname);
            System.out.println("NAME = "+lastname);
            System.out.println("AGE = "+age);
            System.out.println("BIRTHDAY = "+birthday);
            }
            
          
           
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
