/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sqlite01;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ModG
 */
public class InsertCompany {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn=DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String sql = "INSERT INTO COMPANY(ID,NAME,AGE,ADDRESS,SALARY)"
                    +"VALUES(1,'Paul',32,'California',20000.00);";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO COMPANY(ID,NAME,AGE,ADDRESS,SALARY)"
                    +"VALUES(2,'P',22,'Thailand',30000.00);";
            stmt.executeUpdate(sql);
            conn.commit();
           
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
